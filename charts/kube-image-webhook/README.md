# Kube Image Webhook

This chart provides a deployment of the [Kube Image Webhook](https://gitlab.com/autokubeops/kube-image-webhook).

## Requirements

- Helm 3
- Kubernetes 1.16+

## Configuration

| Parameter                                        | Description                                                             | Default                          |
|--------------------------------------------------|-------------------------------------------------------------------------|----------------------------------|
| `replicaCount`                                   | Number of pods to run                                                   | `3`                              |
| `image.registry`                                 | Registry to pull from                                                   | `registry.gitlab.com`            |
| `image.repository`                               |                                                                         | `autokubeops/kube-image-webhook` |
| `image.tag`                                      |                                                                         | ``                               |
| `image.pullPolicy`                               |                                                                         | `IfNotPresent`                   |
| `imagePullSecrets`                               | Secrets to pull the image from a private registry                       | `[]`                             |
| `podAnnotations`                                 | Pod annotations                                                         | `{}`                             |
| `config.images`                                  | Kube Image Webhook configuration                                        | `[]`                             |
| `nodeSelector`                                   | Node labels for pod assignment                                          | `{}`                             |
| `tolerations`                                    | List of node taints to tolerate                                         | `[]`                             |
| `mutatingWebhookConfiguration.enabled`           | Enable or disable the MutatingWebhookConfiguration                      | `true`                           |
| `mutatingWebhookConfiguration.namespaceSelector` | Selector to restrict the namespaces the webhook will watch.             | `{}`                             |
| `mutatingWebhookConfiguration.objectSelector`    | Selector to restrict the objects that the webhook will watch.           | `{}`                             |
| `certs.secretName`                               | Name of the Secret containing `tls.crt` and `tls.key`                   |                                  |
| `certs.caBundle`                                 | Base64-encoded Certificate Authority used to generate TLS certificates. |
| `certmanager.enabled`                            | Use Cert Manager for certificate management.                            | `false`                          |
| `certmanager.issuer.name`                        | Name of the Cert Manager Issuer or ClusterIssuer to use.                |                                  |
| `certmanager.issuer.kind`                        | Kind of the Cert Manager Issuer (e.g. Issuer or ClusterIssuer)          |                                  |
| `logLevel`                                       | Set the logging level. Higher is more.                                  | `1`                              |                                  |

### Images

```yaml
images:
  - source: index.docker.io # catchall for docker.io and library images
    destination: registry.example.org
  - source: quay.io
    destination: harbor.corp.internal/quay.io
```

### Certificates

> Note: `certs.*` and `certmanager.*` are mutually exclusive and Cert Manager will be prioritised over standard cert provisioning.

Using Cert Manager is highly recommended:

```yaml
certmanager:
  enabled: true
  issuer:
    name: my-issuer
    kind: ClusterIssuer
```