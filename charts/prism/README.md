# Prism

[Prism](https://gitlab.com/av1o/go-prism/) is an application designed for caching project dependencies.

## TL;DR

```console
$ helm repo add av1o https://av1o.gitlab.io/charts
$ helm install prism av1o/prism
```

## Introduction

This chart bootstraps a deployment of Prism on a [Kubernetes](https://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes 1.12+
- Helm 3

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release av1o/prism
```

The command deploys Prism on the Kubernetes cluster in the default configuration.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

## Parameters

The following table lists the configurable parameters of the Prism chart, and their default values.
See [values.yaml](./values.yaml) for extended configuration and options.

| Parameter | Description | Default |
| --- | --- | --- |
| `replicaCount` | Number of pods to run | 1 |
| `image.registry` | Docker image registry | `registry.gitlab.com` |
| `image.repository`| Docker image name | `av1o/go-prism` |
| `image.pullPolicy` | Docker image pull policy | `IfNotPresent` |
| `image.tag` | Docker image tag | `${CHART_VERSION}` |
| `imagePullSecrets` | Specify Image pull secrets | `[]` |
| `podAnnotations` | Map of annotations to add to the pods | See `values.yaml` |
| `podSecurityContext` | Map of security context to add to the pod | See `values.yaml` |
| `securityContext` | Map of security context to add to the container | See `values.yaml` |
| `service.type` | Service type | `ClusterIP` |
| `config` | Prism custom config | See `values.yaml` |
| `backend.s3.enabled` | Enables the S3 storage backend | `false` |
| `backend.s3.endpoint` | Override the S3 endpoint | `https://s3.amazonaws.com` |
| `backend.s3.region` | Set the S3 region | `us-west-1` |
| `backend.s3.bucket` | Set the S3 bucket name | `` |
| `backend.s3.pathStyle` | Whether to use S3 domain or path style. Path style is useful in non-AWS S3 deployments (e.g. Minio). | `false` |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.