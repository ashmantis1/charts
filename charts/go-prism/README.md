# Go Prism Helm Chart

This chart provides a deployment of [Prism](https://gitlab.com/go-prism).
Prism is an application designed for caching application packages including Java (Gradle and Maven), NPM, Debian, Alpine, GoLang and static files.

This chart is still in development and may be missing some configuration options.
The deployed instance of Prism is still in development and should not be used for production.

## Requirements

> Helm 2 is not supported.

- Helm 3
- Kubernetes 1.18+

## Configuration

### Globals

| Parameter                     | Description | Default                            |
| ---                           | ---         | ---                                |
| `global.image.pullPolicy` | Set the pullPolicy for all components. | `IfNotPresent` |
| `global.ingress.host` | Set the Host for all Ingress objects. | `prism.example.org` |
| `global.ingress.annotations` | Annotations to be set on all Ingress objects. | See `values.yaml` |
| `global.metrics.enabled` | Enables Prometheus metrics. | `false` |
| `global.metrics.serviceMonitor.enabled` | Enables the creation of a Prometheus ServiceMonitor. | `false` |
| `global.prism.masterKey` | Master key used for encrypting data. | *(random 16 characters)* |
| `global.prism.logging.debug` | Enables debug logging for all components. | `false` |
| `global.prism.logging.json` | Enables JSON-formatted logging output for all components. | `false` |

### API

The API is the user-facing component of Prism.
It provides a REST interface for the Web UI as well as proxying requests to the Reactor and enabled plugins (e.g. GoProxy). 
The API communicates with the Reactor over a gRPC connection. It communicates with plugins over HTTP.

Database credentials are stored in a Kubernetes secret.

| Parameter | Description | Default |
| --- | --- | --- |
| `api.prism.reactorURL` | Address of the Reactor. | `prism-reactor:8080` |
| `api.prism.goproxyURL` | *(disabled by default)* Address of the GoProxy. | `""` |
| `api.prism.dsn` | PostgreSQL DSN. | `""` |
| `api.prism.adminUser` | Full name of the initial administrator. | `""` |

### Reactor

The Reactor is the workhorse component of Prism. It is responsible for fetching data from remotes as well as managing caches.
The Reactor communicates with the API over a gRPC connection.

S3 credentials are stored in a Kubernetes secret.

| Parameter | Description | Default |
| --- | --- | --- |
| `reactor.prism.serviceURL` | Address of the API. | `prism-api:8080` |
| `reactor.prism.masterKey` | Master key used for encrypting data. | *(random 16 characters)* |
| `reactor.s3.bucket` | S3 bucket to store data in. | |
| `reactor.s3.region` | S3 region | `us-east-1` |
| `reactor.s3.pathStyle` | Use path-style buckets rather than DNS-style. Useful for self-hosted installations such as Minio | `false` |
| `reactor.s3.endpoint` | Use a custom S3 endpoint. | |
| `reactor.s3.accessKey` | S3 access key. | |
| `reactor.s3.secretKey` | S3 secret key. | |

### GoProxy

> This plugin is experimental and has performance problems. You should not use it in production yet.

The GoProxy plugin allows Prism to support the `GOPROXY` protocol and natively cache GoLang modules.

If enabled, it will create a Refraction called `go1` containing a remote called `GOPROXY`.
If a Refraction with this name already exists, it will be replaced. The GoProxy plugin will use this Refraction to cache data in the Reactor.

The GoProxy communicates with the API and Reactor over a gRPC connection.

| Parameter | Description | Default |
| --- | --- | --- |
| `goproxy.enabled` | Enables the experimental GoProxy integration. | `false` |
| `goproxy.prism.serviceURL` | Address of the API. | `prism-api:8080` |
| `goproxy.prism.reactorURL` | Address of the Reactor. | `prism-reactor:8080` |
