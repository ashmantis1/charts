# OKD Webhook Helm Chart

This chart provides a deployment of the [OKD Webhook](https://gitlab.com/av1o/okd-webhook).

This chart is still in development and may be missing some configuration options.
The webhook that is deployed is fully-featured and works without issue.

## Requirements

- Helm 3
- Kubernetes 1.18+

## Configuration

| Parameter                     | Description | Default                            |
| ---                           | ---         | ---                                |
| `replicaCount`                  | Number of pods to run | `1`                                |
| `image.registry`                | Registry to pull from | `registry.gitlab.com` |
| `image.repository`              |             | `av1o/okd-webhook` |
| `image.tag`                     |             | ``                           |
| `image.pullPolicy`              |             | `IfNotPresent`                           |
| `imagePullSecrets`              | Secrets to pull the image from a private registry | `[]` |
| `podAnnotations`                | Pod annotations | `{}`                           |
| `nodeSelector`                  | Node labels for pod assignment | `{}` |
| `tolerations`                   | List of node taints to tolerate | `[]` |
| `prometheus.metrics`            | Annotates the service for prometheus auto-discovery. Also denies access to the `/metrics` endpoint from external addresses with Ingress. | `false` |
| `prometheus.path`               | Path to scrape prometheus metrics from | `/metrics` |
| `prometheus.serviceMonitor.enabled`              | Set this to `true` to create ServiceMonitor for Prometheus operator                                                                                                                                                                                                                                                                                                                                                                                                        | `false`                                                       |
| `prometheus.serviceMonitor.additionalLabels`     | Additional labels that can be used so ServiceMonitor will be discovered by Prometheus                                                                                                                                                                                                                                                                                                                                                                                      | `{}`                                                          |
| `prometheus.serviceMonitor.path` | HTTP path to scrape for metrics. | `` |
| `prometheus.serviceMonitor.scheme` | HTTP scheme to use for scraping. | `` |
| `prometheus.serviceMonitor.namespace`            | Optional namespace in which to create ServiceMonitor                                                                                                                                                                                                                                                                                                                                                                                                                       | `nil`                                                         |
| `prometheus.serviceMonitor.interval`             | Scrape interval. If not set, the Prometheus default scrape interval is used                                                                                                                                                                                                                                                                                                                                                                                                | `nil`                                                         |
| `prometheus.serviceMonitor.scrapeTimeout`        | Scrape timeout. If not set, the Prometheus default scrape timeout is used                                                                                                                                                                                                                                                                                                                                                                                                  | `nil`